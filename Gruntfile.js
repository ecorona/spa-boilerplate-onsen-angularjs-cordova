/* eslint-disable camelcase*/
module.exports = function ( grunt ) {
  grunt.initConfig( {
    appConfig: grunt.file.readJSON( 'package.json' ) || {},
    jst: {
      app: {
        files: {
          'www/js/jst.js': require( './scripts/pipeline' ).templates
        }
      }
    },
    'sails-linker': {
      css: {
        options: {
          startTag: '<!--CSS START-->',
          endTag: '<!--CSS END-->',
          fileTmpl: '<link rel="stylesheet" type="text/css" href="%s">',
          appRoot: 'www',
          relative: true
        },
        files: {
          'www/index.html': require( './scripts/pipeline' ).css
        }
      },
      dependencias: {
        options: {
          startTag: '<!--DEPENDENCIES-->',
          endTag: '<!--DEPENDENCIES END-->',
          fileTmpl: '<script src="%s" type="text/javascript" charset="utf-8"></script>',
          appRoot: 'www',
          relative: true
        },
        files: {
          'www/index.html': require( './scripts/pipeline' ).dependencies
        }
      },
      aplicacion: {
        options: {
          startTag: '<!--SCRIPTS-->',
          endTag: '<!--SCRIPTS END-->',
          fileTmpl: '<script src="%s" type="text/javascript" charset="utf-8"></script>',
          appRoot: 'www',
          relative: true
        },
        files: {
          'www/index.html': require( './scripts/pipeline' ).aplicacion
        }
      }
    },
    javascript_obfuscator: {
      options: {
        reservedNames: [
          'ng',
          'cordova',
        ],
        compact: true,
        controlFlowFlattening: false,
        deadCodeInjection: false,
        debugProtection: false,
        debugProtectionInterval: false,
        disableConsoleOutput: true,
        identifierNamesGenerator: 'hexadecimal',
        log: false,
        target: 'browser',
        renameGlobals: false,
        rotateStringArray: true,
        selfDefending: false,
        stringArray: true,
        stringArrayEncoding: [ 'base64', ],
        stringArrayThreshold: 0.75,
        transformObjectKeys: false,
        unicodeEscapeSequence: false,
        sourceMap: false,
        sourceMapMode: 'inline'
      },
      main: {
        src: [
          'www/js/loader.js',
          'www/js/jst.js',
          'www/js/**/*.js',
        ]
      }
    },
    banner: '/*\n  <%=appConfig.name %> - version <%= appConfig.version %> - <%= grunt.template.today("dd-mm-yyyy") %>\n' +
    '  <%=appConfig.description %>\n ' +
    ' <%=grunt.template.today("yyyy")%> Si quieres el código fuente, este no es el lugar correcto.\n*/\n',
    usebanner: {
      grupalBanner: {
        options: {
          position: 'top',
          banner: '<%= banner %>',
          linebreak: true
        },
        files: {
          src: [ 'www/js/*.js', 'www/js/**/*.js', ]
        }
      }
    }
  } );
  grunt.loadNpmTasks( 'grunt-banner' );
  grunt.loadNpmTasks( 'grunt-javascript-obfuscator' );
  grunt.loadNpmTasks( 'grunt-sails-linker' );
  grunt.loadNpmTasks( 'grunt-contrib-jst' );
  grunt.registerTask( 'templates', [
    'jst',
  ] );
  grunt.registerTask( 'encrypt', [
    'javascript_obfuscator',
    'usebanner',
  ] );
  grunt.registerTask( 'link', [
    'sails-linker',
  ] );
  grunt.registerTask( 'prebuild', [
    'jst',
    'javascript_obfuscator',
    'usebanner',
    'sails-linker',
  ] );
};
