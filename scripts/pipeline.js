
module.exports.css = [
  'www/dependencies/onsenui/css/onsenui.css',
  'www/dependencies/onsenui/css/onsen-css-components.css',
  'www/css/index.css',
];

module.exports.dependencies = [
  'www/dependencies/angular/angular.min.js',
  'www/dependencies/angular-animate/angular-animate.min.js',
  'www/dependencies/angular-route/angular-route.min.js',
  'www/dependencies/angular-sanitize/angular-sanitize.min.js',
  'www/dependencies/onsenui/js/onsenui.min.js',
  'www/dependencies/angularjs-onsenui/dist/angularjs-onsenui.min.js',
];

module.exports.aplicacion = [
  'www/js/jst.js',
  'www/js/loader.js',
  'www/js/*.js',
  'www/js/modules/**/*.js',
];

module.exports.templates = [
  'www/js/modules/**/*.html',
];
