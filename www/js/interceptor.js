ng.config( [
  '$provide',
  '$httpProvider',
  function ( $provide, $httpProvider ) {
    $provide.factory( 'interceptor', [
      '$q',
      '$rootScope',
      function ( $q, $rootScope ) {
        return {
          request: function ( config ) {
            if ( !config || !config.headers ) { config.headers = {}; }
            // si hay un token jwt, adjuntarlo tambien a los headers!
            if ( $rootScope.app.access_token ) {
              config.headers['Authorization'] = `Bearer ${$rootScope.app.access_token}`;
            }
            return config;
          },
          requestError: function ( rejection ) {
            return $q.reject( rejection );
          },
          response: function ( response ) {
            return response;
          },
          responseError: function ( rejection ) { // si hubo un error en la solicitud
            if ( typeof rejection.headers === 'function' ) {
              var headers = rejection.headers();
              if ( headers && headers['x-exit-description'] ) {
                navigator.notification.alert( headers['x-exit-description'], null, 'Problema', [ 'ok', ] );
              }
            }
            return $q.reject( rejection );
          }
        };
      },
    ] );

    $httpProvider.interceptors.push( 'interceptor' );
  },
] );
