ng.controller( 'DashboardController', [
  '$scope',
  '$rootScope',
  '$timeout',
  'DashboardService',
  function ( $scope, $rootScope, $timeout, DashboardService ) {

    $scope.$on( '$viewContentLoaded', async () => {
      console.log( 'DashboardController loaded.' );
      $scope.cargando = true;
      DashboardService.getCards().then( ( response ) => {
        $scope.cards = response.data;
        $scope.error = '';
      } ).catch( ( error ) => {
        console.log( 'DashboardController->$viewContentLoaded->DashboardService.getCards->error:', error );
        $scope.error = 'No se pudo obtener la lista de tarjetas.';
      } ).finally( () => {
        $scope.cargando = false;
      } );
    } );

    $scope.updateTitle = function ( $event ) {
      $rootScope.app.pageTitle = angular.element( $event.tabItem ).attr( 'label' );
    };

    $scope.showDialog = function () {
      console.log( '!' );
      if ( $scope.dialog ) {
        $scope.dialog.show();
      } else {
        ons.createElement( 'dialog.html', { parentScope: $scope, append: true } ).
          then( ( dialog ) => {
            $scope.dialog = dialog;
            dialog.show();
          } );
      }
    };


    $scope.foo = 'bar';
  },
] );
