ng.factory( 'DashboardService', [
  '$http',
  ( $http ) => {
    return {
      getCards: () => {
        return $http.get( 'data/cards.json' );
      }
    };
  },
] );
