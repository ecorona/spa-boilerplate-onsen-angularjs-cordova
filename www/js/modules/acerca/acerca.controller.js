ng.controller( 'AcercaController', [
  '$scope',
  '$rootScope',
  function ( $scope, $rootScope ) {

    $scope.$on( '$viewContentLoaded', () => {
      $rootScope.app.pageTitle = 'Acerca de';

      let states = {};
      states[Connection.UNKNOWN] = 'Desconocido';
      states[Connection.ETHERNET] = 'Ethernet';
      states[Connection.WIFI] = 'WiFi';
      states[Connection.CELL_2G] = 'Cell 2G';
      states[Connection.CELL_3G] = 'Cell 3G';
      states[Connection.CELL_4G] = 'Cell 4G';
      states[Connection.CELL] = 'Cell';
      states[Connection.NONE] = 'Sin conexión';
      $scope.connection = states[navigator.connection.type];

    } );
  },
] );
