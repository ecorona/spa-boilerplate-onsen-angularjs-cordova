ng.controller( 'PruebasController', [
  '$scope',
  function ( $scope ) {

    $scope.$on( '$viewContentLoaded', () => {
      console.log( 'PruebasController loaded.' );
    } );

    $scope.foo = 'bar';
  },
] );
