ng.controller( 'LoginController', [
  '$scope',
  '$location',
  function ( $scope, $location ) {
    $scope.login = {
      username: '',
      password: '',
      rememberme: false
    };

    $scope.$on( '$viewContentLoaded', () => {
      console.log( 'LoginController loaded.' );
    } );

    $scope.doLogin = function () {
      $location.url( '/dashboard' );
    };
  },
] );
