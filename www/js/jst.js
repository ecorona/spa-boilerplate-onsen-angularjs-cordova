this["JST"] = this["JST"] || {};

this["JST"]["www/js/modules/acerca/acerca.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<ons-page ng-controller="AcercaController">\n  <ons-toolbar>\n    <div class="left">\n      <ons-toolbar-button ng-click="mySplitter.left.open()">\n        <ons-icon icon="md-menu"></ons-icon>\n      </ons-toolbar-button>\n    </div>\n    <div class="center">{{app.pageTitle}}</div>\n  </ons-toolbar>\n\n  <p style="text-align: center">\n    <ons-button>\n      {{connection}}\n    </ons-button>\n  </p>\n\n</ons-page>';

}
return __p
};

this["JST"]["www/js/modules/dashboard/dashboard.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<ons-page ng-controller="DashboardController">\n\n  <ons-toolbar>\n    <div class="left">\n      <ons-toolbar-button ng-click="mySplitter.left.open()">\n        <ons-icon icon="md-menu"></ons-icon>\n      </ons-toolbar-button>\n    </div>\n    <div class="center">{{app.pageTitle}}</div>\n  </ons-toolbar>\n\n  <ons-tabbar swipeable position="auto" ons-prechange="updateTitle($event)">\n    <ons-tab page="dashboard.html" label="Dashboard" icon="ion-home, material:md-home" badge="{{cards.length}}" active>\n    </ons-tab>\n    <ons-tab page="dialog.html" label="Dialog" icon="md-settings" active-icon="md-face">\n    </ons-tab>\n  </ons-tabbar>\n\n  <ons-dialog var="dialog">\n    <div style="text-align: center; padding: 10px;">\n      <p>\n        This is a dialog.\n      </p>\n  \n      <p>\n        <ons-button ng-click="dialog.hide()">Close</ons-button>\n      </p>\n    </div>\n  </ons-dialog>\n\n  <ons-bottom-toolbar>\n    <div class="contenido-bottom-toolbar">Contenido</div>\n  </ons-bottom-toolbar>\n\n</ons-page>\n\n<template id="dashboard.html">\n  <ons-page id="dashboard">\n    <p style="text-align: center;">\n      <ons-progress-circular indeterminate ng-if="cargando"></ons-progress-circular>\n    </p>\n    <ons-card ng-repeat="card in cards" ng-if="!cargando">\n      <img ng-src="{{card.image}}" alt="Onsen UI" style="width: 100%">\n      <div class="title">\n        {{card.title}}\n      </div>\n      <div class="content">\n        {{card.description}}\n      </div>\n      <div class="footer" style="text-align: right;">\n        <hr>\n        <ons-button>\n          Ver mas...\n        </ons-button>\n      </div>\n    </ons-card>\n  </ons-page>\n</template>\n\n<template id="dialog.html">\n  <ons-page id="dialog">\n    <p style="text-align: center">\n      <ons-button ng-click="showDialog()">\n        Dialog\n      </ons-button>\n    </p>\n  </ons-page>\n</template>';

}
return __p
};

this["JST"]["www/js/modules/login/login.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<ons-page ng-controller="LoginController">\n\n  <div style="text-align: center; margin-top: 80px">\n    <p style="margin-bottom: 30px;">\n      <img src="img/onsen.svg" alt="" width="50%">\n    </p>\n    <p>\n      <ons-input id="username" modifier="underbar" placeholder="Username" float ng-model="login.username"></ons-input>\n    </p>\n    <p>\n      <ons-input id="password" modifier="underbar" type="password" placeholder="Password" float ng-model="login.password"></ons-input>\n    </p>\n    <p>\n      Recordarme <ons-switch ng-model="login.rememberme"></ons-switch>\n    </p>\n    <p style="margin-top: 30px;">\n      <ons-button ng-click="doLogin()">Acceder</ons-button>\n    </p>\n  </div>\n</ons-page>';

}
return __p
};

this["JST"]["www/js/modules/pruebas/pruebas.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<ons-page ng-controller="PruebasController">\n  <ons-toolbar>\n    <div class="left">\n      <ons-toolbar-button ng-click="mySplitter.left.open()">\n        <ons-icon icon="md-menu"></ons-icon>\n      </ons-toolbar-button>\n    </div>\n    <div class="center">{{app.pageTitle}}</div>\n  </ons-toolbar>\n\n  <p style="text-align: center">\n    <ons-button>\n      {{foo}}\n    </ons-button>\n  </p>\n\n</ons-page>';

}
return __p
};