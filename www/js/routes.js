// rutas
ng.config( [
  '$routeProvider',
  '$locationProvider',
  function ( $routeProvider, $locationProvider ) {
    $locationProvider.hashPrefix( '' );
    $routeProvider.
    when( '/pruebas', {
      controller: 'PruebasController',
      template: JST['www/js/modules/pruebas/pruebas.html'](),
      title: 'Pruebas'
    } ).
    when( '/login', {
      controller: 'LoginController',
      template: JST['www/js/modules/login/login.html'](),
      title: 'Acceso'
    } ).
    when( '/acerca', {
      controller: 'AcercaController',
      template: JST['www/js/modules/acerca/acerca.html'](),
      title: 'Acerca de...'
    } ).
    when( '/dashboard', {
      controller: 'DashboardController',
      template: JST['www/js/modules/dashboard/dashboard.html'](),
      title: 'Dashboard'
    } ).
    otherwise( {
      redirectTo: '/login'
    } );
  },
] );

// control de rutas
ng.run( [
  '$window',
  '$rootScope',
  '$timeout',
  '$location',
  function ( $window, $rootScope, $timeout, $location ) {
    // console.log( 'APP: routes events' );
    $rootScope.app.goToPage = function ( ruta ) {
      $location.url( ruta );
    };
    $rootScope.app.goBack = function () {
      console.log( 'ROUTES->goBack' );
      if ( $rootScope.app.backEvent ) {
        $rootScope.$broadcast( 'back-event', $rootScope.app.backEvent );
        $rootScope.app.backEvent = false;
        return;
      }
      $window.history.back();
    };

    document.addEventListener( 'backbutton', () => {
      $timeout( () => {
        $rootScope.app.goBack();
      } );
    } );

    // verificar rutas
    $rootScope.$on( '$routeChangeStart', ( event, next ) => {

      const freeRides = [
        '/dashboard',
        '/acerca',
        '/login',
        '/pruebas',
      ];

      // las rutas en freeRides siempre entran
      if ( ( next.$$route && next.$$route.originalPath && freeRides.indexOf( next.$$route.originalPath ) > -1 ) ) {
        return true;
      }

      return false;
    } );

    // la ruta ha cambiado
    $rootScope.$on( '$routeChangeSuccess', ( event, current ) => {
      $rootScope.mySplitter.left.close();

      if ( current && current.$$route && current.$$route.originalPath ) {
        $timeout( () => {
          $rootScope.app.rutaActual = current.$$route.originalPath;
        }, 500 );
      }

      // cambiamos el titulo si es que viene uno con esta vista
      if ( current && current.$$route && current.$$route.title ) {
        $rootScope.app.pageTitle = current.$$route.title;
      }

    } );
  },
] );
