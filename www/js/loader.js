// eslint-disable-next-line no-unused-vars, no-redeclare
const ng = angular.module( 'onsen-app',
  [
    'onsen',
    'ngRoute',
    'ngSanitize',
    'ngAnimate',
  ]
).run( [
  '$rootScope',
  '$timeout',
  function ( $rootScope, $timeout ) {
    // global para la app
    $rootScope.app = {
      name: 'Onsen App',
      pageTitle: ''
    };

    // resume event
    document.addEventListener( 'resume', ( event ) => {
      $timeout( () => {
        $rootScope.$broadcast( 'resume', event );
      } );
    } );

    // pause event
    document.addEventListener( 'pause', () => {
      $timeout( () => {
        $rootScope.$broadcast( 'pause' );
      } );
    } );

    // ocultamos el splash screen
    navigator.splashscreen.hide();

  },
] );

// bootstrap angularjs app
document.addEventListener( 'deviceready', () => {
  // estilizamos la barra de la aplicacion
  // poner la barra en azul
  StatusBar.backgroundColorByHexString( '#0064e0' );
  // contenido blanco
  StatusBar.styleLightContent();

  angular.bootstrap( document, [ 'onsen-app', ] );
}, false );
